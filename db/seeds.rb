# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
def seed_customer
  request = CustomerSeed::CUSTOMER_SEED
  Customer.transaction do
    request.each do |customer_request|
      customer_request = customer_request.with_indifferent_access
      customer = CustomerService.add_customer customer_request
    end
  end
end
seed_customer unless Customer.first.present?

def seed_order
  request = OrderSeed::ORDER_SEED
  Order.transaction do
    request.each do |order_request|
      OrderService.add_order order_request
    end
  end
end
seed_order unless Order.first.present?
