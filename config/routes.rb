Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    root 'admin#index'
  # root :to => 'apipie/apipies#index', constraints: lambda { |request| Rails.env.development? }
  namespace :api, defaults: {format: 'json'} do
    post '/statistic', to: 'admin#statistic'
  end

  get '/statistic/:entity', to: 'admin#statistic'

end
