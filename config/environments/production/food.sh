# Run as root
#if [ $EUID -ne 0 ]; then
#  echo "You must have root privilege to perform this operation" 
#  exit 1
#fi

# Install required dependencies on top of Ubuntu
echo "Installing build-essential."
sudo apt-get update
sudo apt-get install -y build-essential
if [ $? -ne 0 ]; then
  echo "Failed to install build-essential to install please try 'sudo apt-get install -y build-essential'"
  exit 1
fi

# Installing required dependencies cmake.
echo "Installing cmake."
sudo apt-get install -y cmake
if [ $? -ne 0 ]; then
  echo "Failed to install cmake to install please try 'sudo apt-get install -y cmake'"
  exit 1
fi

# Installing required dependencies libgtk2.0-dev.
echo "Installing libgtk2.0-dev."
sudo apt-get install -y libgtk2.0-dev
if [ $? -ne 0 ]; then
  echo "Failed to install libgtk2.0-dev to install please try 'sudo apt-get install -y libgtk2.0-dev'"
  exit 1
fi

# Installing required dependencies pkg-config.
echo "Installing pkg-config."
sudo apt-get install -y pkg-config
if [ $? -ne 0 ]; then
  echo "Failed to install pkg-config to install please try 'sudo apt-get install -y pkg-config'"
  exit 1
fi

# Installing required dependencies libavcodec-dev libavformat-dev libswscale-dev.
echo "Installing sudo apt-get install -y libavcodec-dev libavformat-dev libswscale-dev."
sudo apt-get install -y libavcodec-dev libavformat-dev libswscale-dev
if [ $? -ne 0 ]; then
  echo "Failed to install libavcodec-dev libavformat-dev libswscale-dev to install please try 'sudo apt-get install -y libavcodec-dev libavformat-dev libswscale-dev'"
  exit 1
fi

# Installing required dependencies libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev.
echo "Installing sudo apt-get install -y libavcodec-dev libavformat-dev libswscale-dev."
sudo apt-get install -y libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev
if [ $? -ne 0 ]; then
  echo "Failed to install libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev to install please try 'sudo apt-get install -y libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev'"
  exit 1
fi

# Installing required dependencies.
echo "Installing python dependencies ."
sudo apt-get -qq install libopencv-dev build-essential checkinstall cmake pkg-config yasm libjpeg-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libxine2 libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev python-dev python-numpy libtbb-dev libqt4-dev libgtk2.0-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils python-opencv
sudo apt-get update
if [ $? -ne 0 ]; then
  echo "Failed to install python dependencies"
  exit 1
fi

# C/C++ compiler support, Zlib development headers, OpenSSL development headers, GNU Readline development headers
echo "Installing required dependencies"
echo "apt-get install -y build-essential zlib1g-dev libssl-dev libreadline-dev libmysqlclient-dev"
sudo apt-get install --yes --force-yes build-essential zlib1g-dev libssl-dev libreadline-dev libmysqlclient-dev
if [ $? -ne 0 ]; then
  echo "Failed to install dependencies"
  read -p "Press ENTER to continue, press ^C to quit" x
  exit 1
fi

# We are going to need to have git on there too
which git
if [ $? -ne 0 ]; then
  echo "Installing git"
  sudo apt-get install -y git
  if [ $? -ne 0 ]; then
    echo "Failed to install git"
    echo "Make sure 'apt-get install git' runs successfully and try again"
    exit 1
  fi
fi

# Install rvm to manage rubies
which rvm
if [ $? -ne 0 ]; then
  echo "Installing rvm Ruby version manager"
  sudo \curl -sSL https://get.rvm.io | bash
  source ~/.rvm/scripts/rvm
  rvm install 2.3.1
  if [ $? -ne 0 ]; then
    echo "Failed to install rvm"
    echo "Look at rvm installation web page https://rvm.io/rvm/install"
    exit 1
  fi
  source /etc/profile.d/rvm.sh
fi

# Install ruby-2.3.1 using rvm
which ruby
if [ $? -ne 0 ]; then
  echo "Installing ruby-2.3.1"
  rvm install 2.3.1
  if [$? -ne 0]; then
    echo "Failed to install ruby-2.3.1"
    echo "Try troublshooting or other version of ruby"
    exit 1
  fi
fi

# Set 2.3.1 as a current and default version of ruby
rvm use 2.3.1 --default
if [ $? -ne 0 ]; then
  echo "Failed to set ruby default version to ruby-2.3.1"
  read -p "Press ENTER to continue, press ^C to quit" x
fi

#Install nodejs
which nodejs
if [ $? -ne 0 ]; then
  curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
  sudo apt-get install -y nodejs
  if [ $? -ne 0 ]; then
    echo "Failed to install nodejs"
    echo "Make sure 'apt-get install -y nodejs' runs successfully and try again"
    exit 1
  fi
  which node
  if [ $? -ne 0 ]; then
    sudo ln -s /usr/bin/nodejs /usr/bin/node
  fi
fi

#Check if npm:javascript package manager is installed or not
which npm
if [ $? -ne 0 ]; then
  echo "npm is not available"
  echo "Make sure you nodejs installation was successfull and soft link to node is created"
  exit 1
fi

#Install bower: angular package manager
which bower
if [ $? -ne 0 ]; then
  sudo npm install -g bower
  if [ $? -ne 0 ];then
    echo "Failed to install bower"
    echo "Make sure 'sudo npm install -g  bower' runs successfully and try again"
  fi
fi


#Install stable version of nginx
which nginx
if [ $? -ne 0 ]; then
  echo "Installing nginx"
  sudo add-apt-repository ppa:nginx/stable
  sudo apt-get update
  sudo apt-get install -y nginx
  if [ $? -ne 0 ]; then
    echo "Failed to install nginx"
    echo "Make sure 'apt-get install nginx' runs successfully and try again"
    exit 1
  fi
fi

# Auto start nginx on boot
sudo chmod -R 775 /etc/init.d/nginx
sudo /usr/sbin/update-rc.d -f nginx defaults
if [ $? -ne 0 ]; then
  echo "Failed to configure nginx"
  echo "Make sure you have permissions"
  read -p "Press ENTER to continue, press ^C to quit" x
fi

# Craete /var/tmp/nginx derectory
sudo mkdir -p /var/tmp/nginx
if [ $? -ne 0 ]; then
  echo "Unable to create /var/tmp/nginx"
  echo "Make sure you have required permissions"
  exit 1
fi

# Start nginx
sudo service nginx start
if [ $? -eq 2 ]; then
  echo "Failed to start nginx"
  echo "Make sure 'service nginx start' runs successfully"
  read -p "Press ENTER to continue, press ^C to quit" x
fi

# Generate public key to be used to clone git repository
read -p "Do you wish to generate deployment key? (y/n) " yn
case $yn in
    [Yy]* ) sudo ssh-keygen -t rsa;
            if [ $? -ne 0 ]; then
              echo "Unable to generate deployment key"
              echo "Look at git ssh-keygen document to troublshoot"
              exit 1
            fi;
            echo "ssh key generated successfully";
            read -p "Copy ssh key to you account and press ENTER to continue" x;;
    [Nn]* ) echo "Continue...";;
    * ) echo "Continue...";;
esac

# Create www directory for application if not present
if [ ! -d /var/www ]; then
  sudo mkdir -p /var/www && chmod -R 776 /var/www
  if [ $? -ne 0 ]; then
    echo "Unable to create /www directory"
  echo "Make sure you have required permissions"
    exit 1
  fi
else
  cd /var/www
fi

# Clone or pull git repository in /www/ directory
if [ ! -d food-exchange ]; then
  sudo git clone git@gitlab.com:dumbre/food-exchange.git && cd food-exchange
else
  cd dapper-don
  sudo git checkout .
  sudo git pull
fi
if [ $? -ne 0 ]; then
  echo "Failed to fetch repository"
  echo "Make sure you have required permissions"
  exit 1
fi

#Create pids and log directoy
sudo mkdir pids log
sudo chown -R deployer:deployer .
sudo chmod -R 777 .
sudo chmod 777 pids
sudo chmod 777 log
sudo chmod 666 db/schema.rb
sudo chmod 666 Gemfile.lock
sudo chmod 666 vendor/assets/bower.json
sudo chmod 666 vendor/assets/.bowerrc
sudo chmod 777 public

# Running bundle install
which bundler
if [ $? -ne 0 ]; then
  echo "Installing rvm Ruby version manager"
  sudo apt-get install -y bundler
  if [ $? -ne 0 ]; then
    echo "Failed to install bundler"
    echo "Make sure 'apt-get install bundler' runs successfully and try again"
    exit 1
  fi
fi
bundle install
if [ $? -ne 0 ]; then
  echo "Failed to install gems through bundle install"
  echo "Make sure 'bundle install' runs successfully in app directory and try again"
  exit 1
fi

#Run bower
bundle exec rake bower:install
if [ $? -ne 0 ]; then
  echo "Failed to install bower packages, please check your package list"
  echo "Make sure 'rake -T bower' runs successfully in app directory and try again"
  exit 1
fi

# Run database migrations
echo "Creating database and running migrations..."
bundle exec rake db:create db:migrate db:seed RACK_ENV=production
if [ $? -ne 0 ]; then
  echo "Failed to create database or migrate. Look for errors to troubleshoot"
  exit 1
fi

# Remove the default configuration file and create new configuration file
if [ -f  /etc/nginx/sites-available/default ]; then
  sudo rm -f /etc/nginx/sites-available/default
fi
if [ ! -f  /etc/nginx/conf.d/default.conf ]; then
  sudo cp config/environments/production/nginx.conf /etc/nginx/conf.d/default.conf
  if [ $? -ne 0 ]; then
    echo "Failed to create configuration file"
    exit 1
  fi
fi

# Precompile assets for production deployment
RAILS_ENV=production bundle exec rake assets:precompile
if [ $? -ne 0 ]; then
  echo "Failed to execute assets:precompile"
  echo "Make sure 'RAILS_ENV=production bundle exec rake assets:precompile' runs successfully in your application root and try again"
  exit 1
fi

sudo chown -R deployer:deployer tmp
sudo chown -R deployer:deployer public
sudo chmod -R 777 tmp
sudo chmod -R 777 public

# Kill unicorn master process if already running
if ps -aux | grep unicorn; then
  if [ -f  pids/unicorn.pid ]; then
    sudo kill -9 `cat pids/unicorn.pid`
  fi
fi

# Start Unicorn
unicorn -c unicorn.rb -E production -D
if [ $? -ne 0 ]; then
  echo "Failed to start unicorn"
  echo "Make sure 'unicorn -c unicorn.rb -E production -D' runs successfully and try again"
  exit 1
fi

# Restart nginx
sudo service nginx restart
if [ $? -ne 0 ]; then
  echo "Failed to start nginx"
  echo "Make sure 'service nginx restart' runs successfully"
  exit 1
fi

echo "Congratulations!!!  Your application is up and running..."
