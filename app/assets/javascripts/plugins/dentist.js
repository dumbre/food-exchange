!(function(){
  var undefined;

  var DEFAULTS = {
    delimiter: '&',
    keyValueSeparator: '=',
    startAfter: '?',
  };

  String.prototype.extract = function(opts){
    function filterInt(value) {
        return (/^(\-|\+)?([0-9]+|Infinity)$/.test(value))? Number(value) : NaN;
    }
    function arraykey(key) {
        return key.split('[]')[0];
    }

    var opts = opts || {},
      keyValuePairs = [],
      params = {};

    if (this.length <= 1)
      return params;

    var delimiter = opts.delimiter || DEFAULTS.delimiter;
    var keyValueSeparator = opts.keyValueSeparator || DEFAULTS.keyValueSeparator;
    var startAfter = opts.startAfter || DEFAULTS.startAfter;
    var limit = filterInt(opts.limit) >= 1? opts.limit : undefined;

    var querystringStartIndex = this.lastIndexOf(startAfter) + 1;
    var keyValueSeparatorFirstIndex = this.indexOf(keyValueSeparator, querystringStartIndex); 

    if (keyValueSeparatorFirstIndex < 0) 
      return params;

    // scope of finding params only applicable to str
    var str = querystringStartIndex < 0? new String(this) : this.substring(querystringStartIndex); 
    
    keyValuePairs = str.split(delimiter, limit);
    for (var i = keyValuePairs.length - 1; i >= 0; i--) {
      if(keyValuePairs[i].startsWith('+')) {
        keyValuePairs[i-1] = keyValuePairs[i-1]+'&'+keyValuePairs[i];
        continue;
      }
      kvPair = keyValuePairs[i].split(keyValueSeparator, 2);
      // ignore any items after first value found, where key = kvPair[0], value = kvPair[1]
      var value = filterInt(kvPair[1])? filterInt(kvPair[1]) : kvPair[1].replace(/\+/g, ' '); // return int if value is parsable;
      var key = arraykey(kvPair[0]);
      if(kvPair[0] !== key) {
        if(params[key] === undefined)
          params[key] = [];
        params[key].push(value);
      } else {
        params[kvPair[0]] = value
      }
    }
    return params;
  };
})();
