// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery2
//= require jquery_ujs
//= require angular
//= require angular-animate
//= require angular-aria
//= require angular-messages
//= require plugins/angular-material
//= require plugins/slider
//= require plugins/angular_pagination
//= require plugins/angular-socialshare
//= require plugins/read_more
//= require plugins/masonry
//= require plugins/pinterest_grid
//= require plugins/angular-chart/chart
//= require plugins/angular-chart/angular-chart
//= require_tree .
angular.module('foodExchange',['ngMaterial', 'ui.select','xeditable','chart.js']).run(function(editableOptions) {
  editableOptions.theme = 'bs3';
})
.service('apiService', ['$http', apiService])
.service('awsService', ['$rootScope', awsService])
.service('asyncApiService', ['$http', asyncApiService])
.controller('adminCtrl', ['$rootScope','$sce', '$scope', 'apiService','asyncApiService', adminCtrl])