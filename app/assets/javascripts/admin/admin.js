var adminCtrl = function($rootScope, $sce, $scope, apiService, asyncApiService) {
   $scope.new_talent_statistics={}
   $scope.new_order_statistics={}
   $scope.cancel_order_statistics={}
   $scope.delay_order_statistics={}
	 $scope.getCustomer = function(timeframe){
	  asyncApiService.get('/statistic/customers?timeframe='+(timeframe||'month'), function(response, error) {
      if(response) {
	      $scope.new_talent_statistics = response;
	  	  console.log($scope.new_talent_statistics);
	      $scope.newTalentLabel = $scope.new_talent_statistics['label'];
        $scope.newTalentData = $scope.new_talent_statistics['data'];
        $scope.$apply();
      }
    });
  };

  $scope.getOrders = function(timeframe){
	  asyncApiService.get('/statistic/orders?timeframe='+(timeframe||'month'), function(response, error) {
      if(response) {
	      $scope.new_order_statistics = response;
	  	  console.log($scope.new_order_statistics);
	      $scope.newOrderLabel = $scope.new_order_statistics['label'];
        $scope.newOrderData = $scope.new_order_statistics['data'];
        $scope.$apply();
      }
    });
  };

  $scope.getDeliverOrders = function(timeframe){
	  asyncApiService.get('/statistic/order_deliver?timeframe='+(timeframe||'month'), function(response, error) {
      if(response) {
	      $scope.new_order_statistics = response;
	  	  console.log($scope.new_order_statistics);
	      $scope.deliverOrderLabel = $scope.new_order_statistics['label'];
        $scope.deliverOrderData = $scope.new_order_statistics['data'];
        $scope.$apply();
      }
    });
  };

  $scope.getCancelOrders = function(timeframe){
	  asyncApiService.get('/statistic/order_cancel?timeframe='+(timeframe||'month'), function(response, error) {
      if(response) {
	      $scope.cancel_order_statistics = response;
	  	  console.log($scope.cancel_order_statistics);
	      $scope.cancelOrderLabel = $scope.cancel_order_statistics['label'];
        $scope.cancelOrderData = $scope.cancel_order_statistics['data'];
        $scope.$apply();
      }
    });
  };
  $scope.getOrdersNotDeliver = function(timeframe){
	  asyncApiService.get('/statistic/order_not_deliver?timeframe='+(timeframe||'month'), function(response, error) {
      if(response) {
	      $scope.delay_order_statistics = response;
	  	  console.log($scope.delay_order_statistics);
	      $scope.delayOrderLabel = $scope.delay_order_statistics['label'];
        $scope.delayOrderData = $scope.delay_order_statistics['data'];
        $scope.$apply();
      }
    });
  };
};