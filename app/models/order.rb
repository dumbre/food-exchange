class Order < ActiveRecord::Base
  	VALID_STATUS = %w(created confirm inprogress deliverd cancel delay)
  
    belongs_to :customers,  class_name: 'Customer'

	  state_machine :status, :initial => :created do
	  event :confirm do
	    transition [:created] => :confirm
	  end

	  event :inprogress do
	    transition [:confirm] => :inprogress
	  end

	  event :deliverd do
	    transition [:inprogress] => [:deliverd]
	  end

	  event :cancel do
	    transition [:created, :confirm] => [:cancel]
	  end

	  event :delay do
	    transition [:confirm, :inprogress] => [:delay]
	  end
	end

	VALID_STATUS.each do |food_exchange_status|
	  define_method("#{food_exchange_status}?") {status == food_exchange_status}
	end
end
