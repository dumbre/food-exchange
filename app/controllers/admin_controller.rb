class AdminController < ApplicationController

  def index
    
  end
 
  def statistic
      conditions = ""
      date_format = ""
      date_column = case params[:entity]
      when 'orders', 'customers'
        'created_at'
      when 'payments', 'job_closures','order_deliver','order_cancel','order_not_deliver'
        'updated_at'
      when 'prime_talents'
        'calendar_date'
      end

      case params[:timeframe]
      when 'week'
        conditions="#{date_column} BETWEEN '#{7.days.ago.strftime("%Y-%m-%d")}' AND '#{-1.day.ago.strftime("%Y-%m-%d")}'"
        date_format = "%Y-%m-%d"
      when 'month'
        conditions="#{date_column} BETWEEN '#{1.month.ago.strftime("%Y-%m-%d")}' AND '#{-1.day.ago.strftime("%Y-%m-%d")}'"
        date_format = "%Y-%m-%d"
      when 'year'
        conditions="#{date_column} BETWEEN '#{1.year.ago.strftime("%Y-%m-%d")}' AND '#{-1.day.ago.strftime("%Y-%m-%d")}'"
        date_format = "%Y-%m"
      end

      result = nil
      case params[:entity]
      when 'orders'
        result = ActiveRecord::Base.connection.select_all("select DATE_FORMAT(#{date_column}, '#{date_format}') unit, count(*) count from #{params[:entity]} where #{conditions} GROUP BY DATE_FORMAT(#{date_column}, '#{date_format}')").as_json
        result = {
          label: result.map{|stat| stat['unit']},
          data: result.map{|stat| stat['count']}
        }
      when 'customers'
        result = ActiveRecord::Base.connection.select_all("select DATE_FORMAT(#{date_column}, '#{date_format}') unit, count(*) count from #{params[:entity]} where #{conditions} GROUP BY DATE_FORMAT(#{date_column}, '#{date_format}')").as_json
        result = {
          label: result.map{|stat| stat['unit']},
          data: result.map{|stat| stat['count']}
        }
      when 'order_deliver'
        result = ActiveRecord::Base.connection.select_all("select DATE_FORMAT(#{date_column}, '#{date_format}') unit, count(*) count from orders where #{conditions} AND status='deliverd' GROUP BY DATE_FORMAT(#{date_column}, '#{date_format}')").as_json
        result = {
          label: result.map{|stat| stat['unit']},
          data: result.map{|stat| stat['count']}
        }
      when 'order_not_deliver'
        result = ActiveRecord::Base.connection.select_all("select DATE_FORMAT(#{date_column}, '#{date_format}') unit, count(*) count from orders where #{conditions} AND status='delay' GROUP BY DATE_FORMAT(#{date_column}, '#{date_format}')").as_json
        result = {
          label: result.map{|stat| stat['unit']},
          data: result.map{|stat| stat['count']}
        }
      when 'order_cancel'
        result = ActiveRecord::Base.connection.select_all("select DATE_FORMAT(#{date_column}, '#{date_format}') unit, count(*) count from orders where #{conditions} AND status='cancel' GROUP BY DATE_FORMAT(#{date_column}, '#{date_format}')").as_json
        result = {
          label: result.map{|stat| stat['unit']},
          data: result.map{|stat| stat['count']}
        }
      end
      render json: (result||{}).merge(timeframe: params[:timeframe])
  end



  private

  def admin_params
    params.permit(:customer_id, :tracking_id, :status, :name, :location, :mobile_number)
  end
end