class OrderService
  class << self

    def add_order(request)
      order_request = request.extract!(:customer_id, :tracking_id, :status)
      order = Order.create! order_request
      order
    end
  end
end