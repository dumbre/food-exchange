class CustomerService
  class << self

    def add_customer(request)
      customer_request = request.extract!(:name, :location, :mobile_number)
      customer = Customer.create! customer_request
      customer
    end
  end
end

